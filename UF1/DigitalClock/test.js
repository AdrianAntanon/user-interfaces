var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d'); 
var segmentDefinitions = [
  // this was simply easier to type
  [0, 1, 2, 4, 5, 6],          // 0
  [2, 5],                      // 1
  [0, 2, 3, 4, 6],             // 2
  [0, 2, 3, 5, 6],             // 3
  [1, 2, 3, 5],                // 4
  [0, 1, 3, 5, 6],             // 5
  [0, 1, 3, 4, 5, 6],          // 6
  [0, 2, 5],                   // 7
  [0, 1, 2, 3, 4, 5, 6],       // 8
  [0, 1, 2, 3, 5, 6],          // 9
];

// onkeydown expects a callback function
window.onkeydown = function(event) {
  var key = +event.key; // parse the key as a number
  if (key >=0 && key <= 9)
    drawNumber(key);
}

// fills the canvas with a white rectangle
function clearCanvas() {
  context.fillStyle = 'white';
  context.fillRect(0, 0, canvas.width, canvas.height);
}

// draws a number onto the canvas
function drawNumber(number) {
  clearCanvas();
  segmentDefinitions[number].forEach(drawSegment)
}

// draws a specific segment onto the canvas
function drawSegment(id) {
  var width = canvas.width;
  var height = canvas.height;
  var borderWidth = 20;
  context.fillStyle = 'red';

  switch(id) {
    case 0:
      context.fillRect(
        0, 0, 
        width, borderWidth);
      break;
    case 1:
      context.fillRect(
        0, 0, 
        borderWidth, height / 2);
      break;
    case 2:
      context.fillRect(
        width - borderWidth, 0, 
        borderWidth, height/2);
      break;
    case 3:
      context.fillRect(
        0, height / 2, 
        width, borderWidth);
      break;
    case 4:
      context.fillRect(
        0, height / 2, 
        borderWidth, height/2);
      break;
    case 5:
      context.fillRect(
        width - borderWidth, height / 2, 
        borderWidth, height/2);
      break;
    case 6:
      context.fillRect(
        0, height - borderWidth, 
        width, borderWidth);
      break;
  }
}