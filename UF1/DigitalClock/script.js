const digitSegments = [
    [1,2,3,4,5,6],
    [2,3],
    [1,2,7,5,4],
    [1,2,7,3,4],
    [6,7,2,3],
    [1,6,7,3,4],
    [1,6,5,4,3,7],
    [1,2,3],
    [1,2,3,4,5,6,7],
    [1,2,7,3,6]
];



document.addEventListener('DOMContentLoaded', function() {
    
    const _hours = document.querySelectorAll('.hours');
    const _minutes = document.querySelectorAll('.minutes');
    const _seconds = document.querySelectorAll('.seconds');
    
    clearClasses = function(childArray) {
        childArray.forEach(element => {
            element.classList.remove("on")
        });
    }

    setValueSectionValue = function(section, value) {
        clearClasses(section[0].querySelectorAll('.segment'));
        clearClasses(section[1].querySelectorAll('.segment'));
        if (value < 10) {
            setDigit(section[0], 0)
            setDigit(section[1], value)
        } else {
            setDigit(section[0], (value - value % 10) / 10)
            setDigit(section[1], value % 10)
        }
    }
    
    setDigit = function(parentDigitElement, value) {
        const childElements = parentDigitElement.querySelectorAll('.segment')
        const digitIndices = digitSegments[value];
        for (const item of digitIndices) {
          childElements[item-1].classList.add("on");
        }  
    }

    let lastTimeStr = ''
    setInterval(function() {
        var date = new Date();
        const currentTimeStr = `${date.getHours()}${date.getMinutes()}${date.getSeconds()}`
        if (lastTimeStr ===  currentTimeStr) {
            return;
        }

        setValueSectionValue(_hours, date.getHours())
        setValueSectionValue(_minutes, date.getMinutes())
        setValueSectionValue(_seconds, date.getSeconds())

        lastTimeStr = `${date.getHours()}${date.getMinutes()}${date.getSeconds()}`;
    }, 100)
});