function digital() {
  var dt = new Date();
  var hrs = dt.getHours();
  var min = dt.getMinutes();
  var sec = dt.getSeconds();

  getDecimal(hrs, sec, min);

  setTimeout(function () {
    digital();
  }, 1000);
}

function getDecimal(hrs, num, min) {
  var sec1 = Math.floor(num / 10);
  var sec2 = num % 10;
  var min1 = Math.floor(min / 10);
  var min2 = min % 10;
  var hr1 = Math.floor(hrs / 10);
  var hr2 = hrs % 10;

  drawTime(hr1, 'canvas-box0');
  drawTime(hr2, 'canvas-box1');
  drawTime(min1, 'canvas-box2');
  drawTime(min2, 'canvas-box3');
  drawTime(sec1, 'canvas-box4');
  drawTime(sec2, 'canvas-box5');
}

function drawTime(sec, stringReloj) {
  var canvas = document.getElementById(stringReloj);
  if (canvas.getContext) {
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, 500, 500);

    const seconds = parseInt(sec);


    switch (seconds) {
      case 1:
        draw1();
        break;
      case 2:
        draw2();
        break;
      case 3:
        draw3();
        break;
      case 4:
        draw4();
        break;
      case 5:
        draw5();
        break;
      case 6:
        draw6();
        break;
      case 7:
        draw7();
        break;
      case 8:
        draw8();
        break;
      case 9:
        draw9();
        break;
      default:
        draw0();
        break;
    }

  }

  function drawA() {
    //A
    ctx.fillStyle = '#3FF05A';
    ctx.fillRect(50, 20, 100, 20);
  }

  function drawB() {
    //B
    ctx.fillStyle = '#3FF05A';
    ctx.fillRect(150, 40, 20, 100);
  }

  function drawC() {
    //C
    ctx.fillStyle = '#3FF05A';
    ctx.fillRect(150, 160, 20, 100);
  }
  function drawD() {
    //D
    ctx.fillStyle = '#3FF05A';
    ctx.fillRect(50, 260, 100, 20);
  }
  function drawE() {
    //E
    ctx.fillStyle = '#3FF05A';
    ctx.fillRect(30, 160, 20, 100);
  }
  function drawF() {
    //F
    ctx.fillStyle = '#3FF05A';
    ctx.fillRect(30, 40, 20, 100);
  }
  function drawG() {
    //G
    ctx.fillStyle = '#3FF05A';
    ctx.fillRect(50, 140, 100, 20);
  }
  function draw1() {
    drawB();
    drawC();
  }
  function draw2() {
    drawA();
    drawB();
    drawG();
    drawE();
    drawD();
  }
  function draw3() {
    drawA();
    draw1();
    drawG();
    drawD();
  }

  function draw4() {
    drawF();
    drawG();
    draw1();
  }
  function draw5() {
    drawA();
    drawF();
    drawG();
    drawC();
    drawD();
  }
  function draw6() {
    draw5();
    drawE();
  }
  function draw7() {
    draw1();
    drawA();
  }
  function draw8() {
    draw6();
    drawB();
  }
  function draw9() {
    draw4();
    drawA();
  }
  function draw0() {
    draw7();
    drawF();
    drawE();
    drawD();
  }
}